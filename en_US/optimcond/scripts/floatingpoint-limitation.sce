// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//**********************************************************
// Draw the limitations of floating point arithmetic
// near the optimum
//
format("e",25)
//**********************************************************
// Univariate function
// x* = 0
// f(x*)=0
// f'(x*)=0
// f''(x*)=2
function y = f ( x )
  y = x**2
endfunction
x = linspace(-1,1,100);
y = f ( x ) ;
plot ( x , y )

linspace(1.e-310,1.e-309,10)
// the display does not work, but the values are correct

x = linspace(-3.e-162,3.e-162,10);
y = f ( x ) ;
[x' y']
sx = 1/min(x)
sy = 1/min(y)
plot ( x*sx , y*sy )

//**********************************************************
// Univariate function
// x* = 0
// f(x*) = 1
// f'(x*) = 0
// f''(x*) = 2
function y = f ( x )
  y = 1 + x**2
endfunction
x = linspace(-1,1,100);
y = f ( x ) ;
plot ( x , y )

// Attempt #1
x = linspace(-3.e-8,3.e-8,100);
y = f ( x ) ;
// [x' y']
// Scale the data to let Open GL manage single precision data
plot ( x*3.e7 , (y-1)*1.e16 , "o" )
// Fix the vertical axis
h = gcf();
h.children.data_bounds = [
   -1.   -1
    1.    10
  ];
xtitle ( "Function f(x)=1 + x^2 in the neighbourhood of x=0" , ..
  "x * 3.e7" , "(y-1)*1.e16" )

// See that the derivative is nonzero
function y = fp ( x )
  y = 2 * x
endfunction

x = linspace(-3.e-8,3.e-8,10);
y = fp ( x );
[x' y']

