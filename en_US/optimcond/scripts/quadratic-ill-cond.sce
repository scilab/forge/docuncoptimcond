// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//**********************************************************
// Draw the contours of a well conditionned quadractic function
//
function f = quadwellcond ( x1 , x2 )
  H = [
    1 0
    0 1
    ]
  x = [x1 x2]'
  f = 0.5 * x' * H * x;
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , quadwellcond , [0.1 0.2 0.4 0.6 0.8])
h = gcf();
h.axes_size = [500 500]

//**********************************************************
// Draw the contours of a ill-conditionned quadractic function
//
function f = quadillcond ( x1 , x2 )
  H = [
    1 0
    0 10
    ]
  x = [x1 x2]'
  f = 0.5 * x' * H * x;
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , quadillcond , [0.1 0.2 0.4 0.6 0.8])
h = gcf();
h.axes_size = [500 500]
h.children.data_bounds = [
   -1.  -1
    1.   1
]

// Draw the function phi(alpha) in the direction of the two eigenvectors
function f = phi1(a)
  H = [
    1 0
    0 10
    ]
  p = [1 0]'
  x = [0 0]'
  nc = size(a,"c")
  for i = 1 : nc
    f(i) = 0.5 * (x + a(i) * p)' * H * (x + a(i) * p)
  end
endfunction
function f = phi2(a)
  H = [
    1 0
    0 10
    ]
  p = [0 1]'
  x = [0 0]'
  nc = size(a,"c")
  for i = 1 : nc
    f(i) = 0.5 * (x + a(i) * p)' * H * (x + a(i) * p)
  end
endfunction
adata = linspace(-1,1,100);
plot ( adata , phi1(adata) , "b-" )
plot ( adata , phi2(adata) , "r-." )
legend ( [ "phi1" "phi2" ] )

//**********************************************************
// Consider the example of Gill, Murray, Wright

function f = quadbadcond ( x1 , x2 )
  x = [x1 x2]
  f = 1 + ( x(1) - 1 )^2 + 1.e-6 * (x(2) - 1)^2
endfunction
x = linspace(0.998,1.002,50);
y = linspace(0,3,50);
contour ( x , y , quadbadcond , 5 )


xopt = [1 1]';
u1 = [1 0]';
u2 = [0 1]';
xt = xopt + 1.e-3 * u1;
xs = xopt + u2;
plot ( xt(1) , xt(2) , "b*" )
plot ( xs(1) , xs(2) , "ro" )

function [ f , g , H ] = qbc ( x )
  f = 1 + ( x(1) - 1 )^2 + 1.e-6 * (x(2) - 1)^2
  g = [
    2 * ( x(1) - 1 )
    2 * 1.e-6 * (x(2) - 1)
    ]
  H = [
    2 0
    0 2*1.e-6
    ]
endfunction

[ f , g , H ] = qbc ( xt );
disp([f norm(g)])
[ f , g , H ] = qbc ( xs );
disp([f norm(g)])

//**********************************************************
// Compute the optimum of a Univariate well-conditionned quadratic,
// where the function value at optimum does not limit the accuracy.

function f = quadwellcond ( x )
  H = 1
  f = 1 + 0.5 * H * (x-1)^2;
endfunction
x = linspace(0,2,100);
plot ( x , quadwellcond ( x ))

function [ f , g , ind ] = optimquad ( x , ind )
  H = 1
  f = 1 + 0.5 * H * (x-1)^2;
  g = H * (x-1)
  format("e",25)
  mprintf("x=%s, f(x)=%s, g(x)=%s\n",string(x),string(f),string(g))
endfunction

// Compute the expected accuracy at optimum
xopt = 1
[ fopt , gopt , ind ] = optimquad ( xopt , 2 )
lambda = 1
xbound = sqrt ( 2 * %eps * fopt ./ lambda ) // 2.107342425544701733D-08
gbound = sqrt ( 2 * %eps * fopt .* lambda ) // 2.107342425544701733D-08
//
// Search the optimum with optim
format(25)
x0 = 2
[ f , g , ind ] = optimquad ( x0 , 2 )
[ fopt , xopt ] = optim ( optimquad , x0 )

[ fopt , gopt , ind ] = optimquad ( xopt , 2 )
// Computed :  xopt = 1, fopt = 1, gopt = 0

// The accuracy is due to the fact that the algorithm uses the gradient.

//**********************************************************
// Compute the optimum of a Univariate ill-conditionned quadratic,
// where the function value at optimum does not limits the accuracy.

function f = quadillcond ( x )
  H = 1.e8
  f = 1 + 0.5 * H * (x-1)^2;
endfunction
x = linspace(0,2,100);
plot ( x , quadillcond ( x ))

function [ f , g , ind ] = optimquad ( x , ind )
  H = 1.e8
  f = 1 + 0.5 * H * (x-1)^2;
  g = H * (x-1)
  format("e",25)
  mprintf("x=%s, f(x)=%s, g(x)=%s\n",string(x),string(f),string(g))
endfunction

// Compute the expected accuracy at optimum
xopt = 1
[ fopt , gopt , ind ] = optimquad ( xopt , 2 )
lambda = 1.e8
xbound = sqrt ( 2 * %eps * fopt ./ lambda ) // 2.107342425544701636D-12
gbound = sqrt ( 2 * %eps * fopt .* lambda ) // 2.107342425544701532D-04
//
// Search the optimum with optim
format(25)
x0 = 2
[ f , g , ind ] = optimquad ( x0 , 2 )
[ fopt , xopt ] = optim ( optimquad , x0 )

[ fopt , gopt , ind ] = optimquad ( xopt , 2 )
// Computed : xopt = 1, fopt = 1, gopt = 0

// The accuracy is due to the fact that the algorithm uses the gradient.

//**********************************************************
// Compute the optimum of a Univariate ill-conditionned quadratic,
// where the function value at optimum really limits the accuracy.
// A case where the conditionning is bad, but the accuracy is good !
// This is because the optimum is at x = 0.
// Then the gradient can be reduced as required down to zero.

function f = quadillcond ( x )
  H = 1.e4
  f = 1 + 0.5 * H * (x-1)^2;
endfunction
x = linspace(0,2,100);
plot ( x , quadillcond ( x ))

function [ f , g , ind ] = optimquad ( x , ind )
  H = 1.e4
  f = 1 + 0.5 * H * (x-1)^2;
  g = H * x
  format("e",25)
  mprintf("x=%s, f(x)=%s, g(x)=%s\n",string(x),string(f),string(g))
endfunction

// Compute the expected accuracy at optimum
xopt = 0
fopt = 1
lambda = 1.e4
xbound = sqrt ( 2 * %eps * fopt ./ lambda )
gbound = sqrt ( 2 * %eps * fopt .* lambda )
//
// Search the optimum with optim
format(25)
x0 = 2
[ f , g , ind ] = optimquad ( x0 , 2 )
[ fopt , xopt ] = optim ( optimquad , x0 )

[ f , g , ind ] = optimquad ( xopt , 2 )


//**********************************************************
// Compute the optimum of a ill-conditionned quadratic,
// where the function value at optimum really limits the accuracy.

function f = quadillcond ( x1 , x2 )
  H = [
    1 0
    0 1.e4
    ]
  x = [x1 x2]'
  f = 1 + 0.5 * x' * H * x;
endfunction
xdata = linspace(-1,1,100);
ydata = linspace(-1,1,100);
contour ( xdata , ydata , quadillcond , [5.e1 2.e2 5.e2 1.e3])

function [ f , g , ind ] = optimquad ( x , ind )
  H = [
    1 0
    0 1.e4
    ]
  if ind == 2 | ind == 4 then
    f = 1 + 0.5 * x' * H * x;
  end
  if ind == 2 | ind == 4 then
    g = H * x
  end
  format("e",25)
  mprintf("x=[%s], f(x)=%s, g(x)=[%s]\n",strcat(string(x), " "),string(f),strcat(string(g), " "))
endfunction

// Compute the expected accuracy at optimum
xopt = [0 0]'
fopt = 1
lambda = [1 1.e4]
xbound = sqrt ( 2 * %eps * fopt ./ lambda )
gbound = sqrt ( 2 * %eps * fopt .* lambda )
//
// Search the optimum with optim
format(25)
x0 = [1 1]'
[ f , g , ind ] = optimquad ( x0 , 2 )
[ fopt , xopt ] = optim ( optimquad , x0 )

[ f , g , ind ] = optimquad ( xopt , 2 )

